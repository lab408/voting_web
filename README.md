# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Summary ###

This is the very begining GUI website to demo our voting smartcontract really work.

### Quick Setup ###

* clone this repo
* $ `cd [repo directory]`
* $ `sudo apt-get install npm`
* $ `npm install`  
  *to check the packages are installed ready use `npm list --depth=0`*
* create a file config.json as following content
```
{
        "Port": "[rpcport]",
        "RPC_user": "[rpcuser in bitcoin.conf]",
        "RPC_password": "[rpcpassword in bitcoin.conf]",
        "IP": "[IP]"
}
```
* $ `node app.js` can run this web service

### Note ###

#### package we use ####
If you want to redo this experiment, there are some packages need to be installed:
	- npm 3.9.3 
	- node v6.2.1 or up
	- Ourcoin (modified by bitcoin)
	
#### Relative path ####
The path of `vote.c` is fixed (written in `./route/commands.js`), which is put in the parent directory of the user's data directory, so you may need to change it. 

#### Other issues ####
make sure to keep mining while the vote is launched, otherwise the data **will not** be written in blockchain.

### Vedio Link ###
Here is our demo vedio: https://youtu.be/N14pY7laeTg
