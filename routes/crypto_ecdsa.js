/* digital signature setting */
// for generate private key
var CryptoJS = require('crypto-js');
var ec_curve = "secp256k1";
var r = require('jsrsasign');
var bigInt = require('big-integer');
// for generate public key
var ecdsa = require('ecdsa');
var CoinKey = require('coinkey');
// for digital signature
const crypto = require('crypto');
var EC = require("elliptic").ec;
var ec = new EC(ec_curve);
const uuidV4 = require('uuid/v4');

// ECDSA signing
var signature_sign = function(private_key) {
    var msg = crypto.randomBytes(64).toString('hex');
    var shaMsg = crypto.createHash("sha256").update(msg).digest();
    var mySign = ec.sign(shaMsg, private_key, {canonical: true});
    return {
        "message": shaMsg,
        "signature": mySign
    };
};

// ECDSA verifying
var signature_verify = function(public_key, shaMsg, mySign) {
    var key = ec.keyFromPublic(public_key, 'hex');//import the public key
    //console.log(key.verify(shaMsg, mySign));
    var result = key.verify(shaMsg, mySign);
    return result;
};

// generate private key and public key
var getECKeyPair = function(password,uuid) {
    var biN, biPrv, biX, biY, charlen, ec, epPub, hPrv, hPub, hX, hY, passhash;
    passhash = CryptoJS.SHA256(password.toString()+uuid.toString()).toString();

    var params = r.crypto.ECParameterDB.getByName(ec_curve);

    biN = params["n"];
    biPrv = bigInt(passhash, 16);
    charlen = params["keylen"] / 4;
    // generate private key
    hPrv = ("0000000000" + biPrv.toString(16)).slice(-charlen);
    /* (decrepted) generate public key
    epPub = params["G"].multiply(biPrv);
    biX = bigInt(epPub.getX());
    biY = bigInt(epPub.getY());
    hX = ("0000000000" + biX.toString(16)).slice(-charlen);
    hY = ("0000000000" + biY.toString(16)).slice(-charlen);
    hPub = "0004" + hX + hY;
    */
    // use coinkey to generate public key from private key
    var ck = new CoinKey(new Buffer(hPrv, 'hex'));
    hPub = ck.publicKey.toString('hex');
    return {
        "ecprvhex": hPrv,
        "ecpubhex": hPub
    };
};

module.exports = {
    signature_sign: signature_sign,
    signature_verify: signature_verify,
    getECKeyPair: getECKeyPair
};