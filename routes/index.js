var express = require('express');
var router = express.Router();
//testserver
const secp256k1 = require('secp256k1');
var request = require('request');
var fs = require('fs');
const sha256=require('sha256');
var credentials = require('./credentials');
var ec_curve = "secp256k1";
var router = express.Router();
const uuidV4 = require('uuid/v4');
var crypto_ecdsa=require('./crypto_ecdsa');
wait=require('wait.for');
//const sqlite3 = require('sqlite3').verbose();
var async = require('async');
const cookieParser = require('cookie-parser');
const crypto = require('crypto');
var EC = require("elliptic").ec;
var ec = new EC(ec_curve);
Struct=require("js-struct").Struct;
var Type=require('js-struct').Type;
var getVar=require('../import');
var bufferReversed=require('buffer-reverse');
var contractTx="";
//var contractTx+=getVar.readContractTx();
contractTx+="";
contractTx+='state';
console.log(contractTx);
/* GET home page. */
router.get('/', function(req, res, next) {
  
  console.log(req.secret);
console.log(req.cookies);
//console.log(res.headers);
if(typeof req.cookies.uuid!='undefined'){
	res.redirect('/commands/' + req.cookies.uuid);
}else{
	console.log('no cookies');
res.render('index', {
  	title: "express"
  });
}
});

router.post('/login', function (req, res) {
  res.redirect('/commands/' + req.body.name);
});
router.get('/aidlogin',function(req,res){

var strTobeHash=req.query.uuid;
strTobeHash+=req.query.publickey;
strTobeHash+=req.query.id_type;
if(req.query.id_type=='2'){
strTobeHash+=req.query.googleemail;
}
var generatedHash=sha256(strTobeHash);	
console.log(generatedHash);
/*var bb=fs.readFileSync(contractTx);
console.log(bb);	*/	
var publicKey=req.query.publickey;
var reversedFirstHalfPub=bufferReversed(Buffer.from(publicKey.slice(2,publicKey.length/2+1),'hex'));
			var reversedSecondHalfPub=bufferReversed(Buffer.from(publicKey.slice(publicKey.length/2+1),'hex'));
			console.log(reversedFirstHalfPub);
			console.log(reversedSecondHalfPub)
			var reversedPub=reversedFirstHalfPub.toString('hex')+reversedSecondHalfPub.toString('hex');
fs.open('./routes/state', 'r', function(status, fd) {
    if (status) {
        console.log(status.message);
        return;
    }
    var buffer = Buffer.alloc(512004);
    fs.read(fd, buffer, 0, 512004, null, function(err, num) {
console.log(buffer);
buffer.swap32();
       // console.log(buffer);
var hashCorrect=0;
var tt=buffer.slice(0,322);

console.log(tt);
let user_count=Type.uint32.read(buffer, 0);
console.log(user_count);
buffer.swap32();
for(let i=0;i<user_count;i++){
let chainData=Type.array(Type.char,194).read(buffer,4+i*194);
let tmpHash=chainData.slice(0,64).join("");
let tmpPublicKey=chainData.slice(65,193).join("");
console.log(tmpPublicKey);
console.log(req.query.publickey);
console.log(tmpHash);
console.log(generatedHash);
if(tmpPublicKey==reversedPub){
	console.log("pubkey correct");
if(tmpHash.slice(0,64)==generatedHash){
console.log(tmpHash);
hashCorrect=1;
}
}
//console.log(tmpAddress);
//console.log(user_count);
}
 if(hashCorrect==1){   
 
 	console.log("hash match!!");

  if(req.query.id_type=='0'){ // process aid login
  var msg = crypto.randomBytes(64).toString('hex');
  var shaMsg = crypto.createHash("sha256").update(msg).digest();
  console.log(req.query);
  var shaHexStr=shaMsg.toString('hex');
  var bufferformmsg=Buffer.from(shaHexStr,'hex');
  //console.log(typeof shaHexStr);
  console.log(shaMsg);
  console.log(bufferformmsg);
  console.log(shaHexStr);
  var signature=ec.sign(Buffer.from(bufferformmsg,'hex'), req.query.publickey, {canonical: true}).toDER();
  console.log(signature.toString());
  res.render('aidlogin',{
    'shaMsg':shaMsg.toString('hex'),
    'publickey':req.query.publickey,
    'uuid':req.query.uuid
  });
}else if(req.query.id_type=='2'){ // process google login
   var passJson=({'uuid':req.query.uuid,'publickey':req.query.publickey,'googleemail':req.query.googleemail});
   var stringpassJson=JSON.stringify(passJson);
   var google_oauth_url = "https://accounts.google.com/o/oauth2/v2/auth?" +
            "scope=email%20profile&" +
            "redirect_uri=" + credentials.google_sign_in.callback_url_login+ "&" +
            "response_type=code&"+"&state="+stringpassJson+
            "&client_id=" + credentials.google_sign_in.client_id;
        res.redirect(google_oauth_url);
}
 }else{
console.log("hash not match");
 	res.redirect('/');
 }
});
});
});
router.get('/google/callback', function(req, res) {
    var code = req.query.code;
    console.log(req.query);
    var token_option = {
        url:"https://www.googleapis.com/oauth2/v4/token",
        method:"POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        form:{
            code: code,
            client_id: credentials.google_sign_in.client_id,
            client_secret: credentials.google_sign_in.client_secret,
            grant_type: "authorization_code",
            redirect_uri: credentials.google_sign_in.callback_url_login
        }
    };
    var uuid=JSON.parse(req.query.state)['uuid'];
    var googleEmail=JSON.parse(req.query.state)['googleemail'];
    request(token_option, function(err, resposne, body) {
        var access_token = JSON.parse(body).access_token;
        var info_option = {
            url:"https://www.googleapis.com/oauth2/v1/userinfo?"+"access_token="+access_token,
            method:"GET",
        };
        request(info_option, function(err, response, body){
            if(err){
                res.send(err);
            }
            else {
              
              console.log(JSON.parse(body)['email']);
              console.log(googleEmail);
              if(JSON.parse(body)['email']==googleEmail+"@gmail.com"){

              res.cookie('googleEmail',googleEmail);
              res.cookie('uuid',uuid);
            }
              res.redirect('/');

            }
          });
      });


  });
router.post('/verifysignature',function(req,res){
var mySign=req.body.signature;
var recoverSig=Buffer.from(mySign,'hex');
bufferMsg=Buffer.from(req.body.shaMsg,'hex');
console.log(req.body.publickey);
console.log(bufferMsg);

console.log(typeof mySign);
console.log(typeof req.body.publickey);
console.log(typeof bufferMsg);
var isTrue=secp256k1.verify(bufferMsg,recoverSig,Buffer.from(req.body.publickey,'hex'))
setTimeout(function(){console.log(isTrue);},3000);

if(isTrue){
  res.cookie('uuid',req.body.uuid);
  console.log('cookie set');
}
setTimeout(function(){res.redirect('/');},3000);
});
module.exports = router;
