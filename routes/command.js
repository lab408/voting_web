var express = require('express');
var router = express.Router();
var getVar = require('../import.js');
var bitcoin_rpc = require('node-bitcoin-rpc');

router.get('/:name/:command', function(req, res) {
	res.render('command', {
  	command_name: req.params.command,
  	name: req.params.name,
  	pathname: 'command',
  	ip: getVar.readIP()
  });
});

var port = getVar.readPort(), rpc_user = getVar.readRPC_user(), rpc_password = getVar.readRPC_password();
var key = "key000";

//post
router.post('/:name/:command', function (req, res) {
  var argvs;
  if (req.params.command == "vote-2") {
  	console.log("post vote-2");
  	bitcoin_rpc.init('localhost', port, rpc_user, rpc_password);
		argvs = [req.body.txid];
		bitcoin_rpc.call('dumpcontractmessage', argvs, function (err, rpc_res) {
		  if (err !== null) {
		    console.log('I have an error :( ' + err + rpc_res.error);
		  } else {
		    console.log('result ' + rpc_res.result);
		    res.render('command', {
			  	command_name: req.params.command,
			  	name: req.params.name,
			  	pathname: 'command',
			  	txid: req.body.txid,
			  	data: rpc_res.result,
			  	ip: getVar.readIP()
			  });
			}
		});
  } else {
	  if (req.params.command == "vote") {
	  	argvs = [req.body.txid, req.params.command, req.params.name, key, req.body.vote_for];
	  } else {
	  	argvs = [req.body.txid, req.params.command, req.params.name, key];
	  }
	  bitcoin_rpc.init('localhost', port, rpc_user, rpc_password);
		bitcoin_rpc.call('callcontract', argvs, function (err, rpc_res) {
		  if (err !== null) {
		    console.log('I have an error :( ' + err + rpc_res.error);
		    res.render('string', {
					data: rpc_res.error,
					name: req.params.name,
					pathname: "command",
					command: "other",
					ip: getVar.readIP()
				});
		  } else {
		    console.log('result ' + rpc_res.result);
		    res.render('string', {
					data: "success!",
					name: req.params.name,
					pathname: "command",
					command: "other",
					ip: getVar.readIP()
				});
			}
		});
	}
});

module.exports = router;
