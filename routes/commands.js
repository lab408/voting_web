var express = require('express');
var router = express.Router();
var getVar = require('../import.js');
var bitcoin_rpc = require('node-bitcoin-rpc');

router.get('/:name', function(req, res) {
  res.render('commands', {
  	name: req.params.name,
  	pathname: 'commands',
  	ip: getVar.readIP()
  });
});

var key = "key000";
var port = getVar.readPort(), rpc_user = getVar.readRPC_user(), rpc_password = getVar.readRPC_password();

//new vote
router.post('/:name/new_vote', function (req, res) {
	bitcoin_rpc.init('localhost', port, rpc_user, rpc_password);
	var argvs = ["./vote.c", "sign_up", req.params.name, key];
	bitcoin_rpc.call('deploycontract', argvs, function (err, rpc_res) {
	  if (err !== null) {
	    console.log('I have an error :( ' + err + rpc_res.error);
	  } else {
	    console.log('result ' + rpc_res.result);
	    res.render('string', {
				data: rpc_res.result,
				name: req.params.name,
				pathname: "command",
				command: "new",
				ip: getVar.readIP()
			});
		  }
	});
	
});

router.post('/:name/result', function (req, res) {
	bitcoin_rpc.init('localhost', port, rpc_user, rpc_password);
	var argvs = [req.body.txid];
	bitcoin_rpc.call('dumpcontractmessage', argvs, function (err, rpc_res) {
	  if (err !== null) {
	    console.log('I have an error :( ' + err + rpc_res.error);
	  } else {
	    console.log('result ' + rpc_res.result);
	    res.render('string', {
				data: rpc_res.result,
				name: req.params.name,
				pathname: "command",
				command: "result",
				ip: getVar.readIP()
			});
		}
	});
	
});

module.exports = router;
